package realm.sqlite.android.mobile.enb.gable.com.realmdemo.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by goffity on 8/6/2015 AD.
 */
public class Address extends RealmObject {

    @PrimaryKey
    private int id;

    private String addrName;

    public String getAddrName() {
        return addrName;
    }

    public void setAddrName(String addrName) {
        this.addrName = addrName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
