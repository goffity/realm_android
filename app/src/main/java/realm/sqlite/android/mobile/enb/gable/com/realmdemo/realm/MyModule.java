package realm.sqlite.android.mobile.enb.gable.com.realmdemo.realm;

import io.realm.annotations.RealmModule;
import realm.sqlite.android.mobile.enb.gable.com.realmdemo.model.Address;
import realm.sqlite.android.mobile.enb.gable.com.realmdemo.model.Person;

/**
 * Created by goffity on 8/6/2015 AD.
 */
@RealmModule(classes = {Person.class, Address.class})
public class MyModule  {
}
