package realm.sqlite.android.mobile.enb.gable.com.realmdemo;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import realm.sqlite.android.mobile.enb.gable.com.realmdemo.model.Person;
import realm.sqlite.android.mobile.enb.gable.com.realmdemo.realm.MyModule;

public class MainActivity extends ActionBarActivity {

    private final String LOG_TAG = "REALM_TEST";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this)
                .name("realm_test.realm")
                .setModules(new MyModule())
                .schemaVersion(1)
                .build();

        Realm realm = Realm.getInstance(realmConfiguration);

        realm.beginTransaction();

        RealmQuery<Person> personRealmQuery = realm.where(Person.class);

        RealmResults<Person> personRealmResults = personRealmQuery.findAll();

        Log.d(LOG_TAG, "Person size: " + personRealmResults.size());

        if (personRealmResults.size() > 0){

            Log.d(LOG_TAG,"1");

            Person person = new Person();

            person.setId(personRealmResults.get(personRealmResults.size()-1).getId()+1);
            person.setName("A "+person.getId());
            person.setAge(30+person.getId());

            Person person1 = realm.copyToRealm(person);
        }else{

        Log.d(LOG_TAG,"2");

        Person person = realm.createObject(Person.class);

        person.setName("A");
        person.setAge(30);

        }

        realm.commitTransaction();

        personRealmResults = personRealmQuery.findAll();

        for (Person p : personRealmResults){
            Log.d(LOG_TAG,"p.getId(): "+p.getId());
            Log.d(LOG_TAG,"p.getName(): "+p.getName());
            Log.d(LOG_TAG,"p.getAge(): "+p.getAge());
        }

        realm.close();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
